const stacks = document.querySelectorAll(".stack");
const allCards = document.querySelectorAll(".cd-card")
const errorMessage = document.getElementById("message-space")
const switchStackButtons = document.querySelectorAll(".button-switch-stack");
const flipButtoms = document.querySelectorAll(".flip-button");
const shuffleButton = document.getElementById("shuffle-button");
const nextButton = document.getElementById("next-button");
const discardButton = document.getElementById("discard-button");
const resetButton = document.getElementById("reset-button")
const spreadButton = document.getElementById("spread-button")
const stackButton = document.getElementById("stack-button")
const galleryButton = document.getElementById("gallery-button")
const gridContainer = document.getElementsByClassName("grid-draggable")
const openAreaDraggable = document.getElementById("open-area-draggable")
const emptySquares = document.querySelectorAll(".grid-draggable__item")
const rejectedCards = []
const cardsDroppedInGrid = []
const cardsDroppedInOpenArea = []
let isStackModified = []
let currentCardActive;
let prevCardActive;
let cardZindex = 0

const state = {
  activeStack: stacks[0],
  setActiveStack: function (value) {
    this.activeStack = value;
  },
};

init();

function init() {
  setUpDefaultValues();
  manageActiveStack();
  handleFlipCard();
  enableDraggable()
}

function setUpDefaultValues() {
  for (let i = 0; i < stacks.length; i++) {
    switchStackButtons[i].innerText = `Stack ${i + 1}`;
    cardsDroppedInGrid[i] = []
    cardsDroppedInOpenArea[i] = []
    rejectedCards[i] = []
    isStackModified[i] = { status: false }
    stacks[i].classList.add(`stack${i + 1}`)
  }
  if (stacks[0].children.length > 20) {
    setButtonsState("disable", spreadButton)
  }
}

function handleFlipCard() {
  for (let i = 0; i < flipButtoms.length; i++) {
    flipButtoms[i].addEventListener("click", () => {
      if (
        !flipButtoms[i].parentElement.parentElement.classList.contains(
          "cd-card__flip"
        )
      ) {
        flipButtoms[i].parentElement.parentElement.classList.add(
          "cd-card__flip"
        );
      } else {
        flipButtoms[i].parentElement.parentElement.classList.remove(
          "cd-card__flip"
        );
      }
    });
  }
}

function manageActiveStack() {
  // state.activeStack.classList.add("stack__active");
  let prevButton;
  let prevStack;
  let currentButtonActive = switchStackButtons[0]
  currentButtonActive.classList.add("button-switch-stack--active");
  const buttons = document.querySelectorAll(".button-switch-stack");

  for (let i = 0; i < stacks.length; i++) {
    buttons[i].addEventListener("click", () => {
      if (
        buttons[i] !== currentButtonActive ||
        stacks[i] !== state.activeStack
      ) {
        prevButton = currentButtonActive;
        currentButtonActive = buttons[i];
        prevStack = state.activeStack;
        state.setActiveStack(stacks[i]);
        prevButton.classList.remove("button-switch-stack--active");
        currentButtonActive.classList.add("button-switch-stack--active");
        state.activeStack.classList.add("stack__active");
        prevStack.classList.remove("stack__active");
      } else if (buttons[i] === currentButtonActive) {
        if (state.activeStack.classList.contains("stack__active")) {
          state.activeStack.classList.remove("stack__active")
        } else {
          state.activeStack.classList.add("stack__active")
        }
      }

      let isInGalleryMode;

      stacks.forEach(stack => {
        if (stack.classList.contains("stack__active--gallery")) {
          isInGalleryMode = true
        }
      })
      if (state.activeStack.children.length > 20) {
        setButtonsState("disable", spreadButton)
      } else if (!isInGalleryMode && !openAreaDraggable.children.length) {
        setButtonsState("enable", spreadButton)
      }
      if (state.activeStack.children.length > 1) {
        setButtonsState("enable", discardButton, nextButton, shuffleButton)
      } else {
        setButtonsState("disable", discardButton, nextButton, shuffleButton)
      }
      if (!state.activeStack.classList.contains("stack__active--gallery")) {
        setButtonsState("disable", stackButton)
        if (state.activeStack.children.length > 1) {
          setButtonsState("enable", nextButton, shuffleButton)
        }
      } else {
        setButtonsState("enable", stackButton)
        setButtonsState("disable", nextButton, shuffleButton)
      }
      if (!isStackModified[i].status && !hasGridCards()) {
        setButtonsState("disable", resetButton)
      } else {
        setButtonsState("enable", resetButton)
      }
      if (openAreaDraggable.children.length) {
        setButtonsState("enable", resetButton)
      }
    });
  }
}

function onCardClicked(event) {
  let card = event.target.parentElement.parentElement;
  if (card.parentNode === state.activeStack) {
    if (!currentCardActive) {
      currentCardActive = card;
      card.classList.add("cd-card--active");
    } else if (currentCardActive === card) {
      currentCardActive = undefined;
      card.classList.remove("cd-card--active");
    } else {
      prevCardActive = currentCardActive;
      currentCardActive = card;
      prevCardActive.classList.remove("cd-card--active");
      currentCardActive.classList.add("cd-card--active");
    }
  } else {
    return
  }
};

function resetCardsFromOpenArea() {
  setButtonsState("enable", spreadButton, galleryButton)
  openAreaDraggable.classList.remove("open-area--active")
  for (let i = 0; i < cardsDroppedInOpenArea.length; i++) {
    if (cardsDroppedInOpenArea[i].length) {
      cardsDroppedInOpenArea[i].forEach(card => {
        // remove flip class if it has 
        if (card.classList.contains("cd-card__flip")) {
          card.classList.remove("cd-card__flip")
        }
        card.style.transform = "";
        disableMoveCards(card)
        enableCardDraggable(card)
        // return each card to the stack it belongs
        stacks[i].appendChild(card)
      })
      cardsDroppedInOpenArea[i] = []
    }
    if (cardsDroppedInGrid[i].length) {
      resetCardsFromGrid()
    }
    if (state.activeStack.classList.contains(`stack${i + 1}`)) {
      if (!isStackModified[i].status) {
        setButtonsState("disable", resetButton)
      }
    }
  }
}

function resetCardsFromGrid() {
  for (let i = 0; i < cardsDroppedInGrid.length; i++) {
    if (cardsDroppedInGrid[i].length) {
      cardsDroppedInGrid[i].forEach(card => {
        // remove flip class if card doesn't belong to the stack that is in gallery mode
        if (!stacks[i].classList.contains("stack__active--gallery")) {
          if (card.classList.contains("cd-card__flip")) {
            card.classList.remove("cd-card__flip")
          }
        }
        if (card.parentNode.classList.contains("open-area")) {
          card.style.transform = ""
          disableMoveCards(card)
        }
        // return each card to the stack it belongs
        stacks[i].appendChild(card)
      })
      if (!state.activeStack.classList.contains("stack__active--gallery")) {
        setButtonsState("enable", spreadButton, galleryButton)
        gridContainer[0].style.right = ""
        gridContainer[0].style.zIndex = ""
        openAreaDraggable.style.zIndex = ""
      }
      setButtonsState("disable", resetButton)
      cardsDroppedInGrid[i] = []
    }
  }
}

function resetCardsFromSpread() {
  rejectedCards.map(stack => {
    if (stack.length) {
      stack.forEach(card => {
        card.classList.remove("cd-card--discard")
        setButtonsState("disable", resetButton)
      })
    }
  })
}

function resetDiscardedCardsFromActiveStack() {
  for (let i = 0; i < stacks.length; i++) {
    if (state.activeStack.classList.contains(`stack${i + 1}`)) {
      isStackModified[i].status = false
      if (rejectedCards[i].length) {
        rejectedCards[i].forEach(card => {
          // When user discard a card in spread mode I only hide it temporaley so here I just need to remove the dicard class
          if (card.classList.contains("cd-card--discard")) {
            card.classList.remove("cd-card--discard")
          } else {
            state.activeStack.appendChild(card)
          }
        })
        rejectedCards[i] = []
        setButtonsState("disable", resetButton)
      }
    }
  }
  if (!state.activeStack.classList.contains("stack__active--gallery")) {
    setButtonsState("enable", discardButton, nextButton, shuffleButton, galleryButton)
  }
}

function resetAllDiscardedCards() {
  for (let i = 0; i < stacks.length; i++) {
    isStackModified[i].status = false
    if (rejectedCards[i].length) {
      rejectedCards[i].forEach(card => {
        // When user discard a card in spread mode I only hide it temporaley so here I just need to remove the dicard class
        if (card.classList.contains("cd-card--discard")) {
          card.classList.remove("cd-card--discard")
        } else {
          stacks[i].appendChild(card)
        }
      })
      rejectedCards[i] = []
      setButtonsState("disable", resetButton)
    }

  }
  setButtonsState("enable", discardButton, nextButton, shuffleButton, galleryButton)
}

function resetSpread() {
  const cards = [...state.activeStack.children]
  state.activeStack.style.marginBottom = ""
  openAreaDraggable.style.zIndex = ""
  enableDraggable()
  cards.forEach(card => card.style.transform = "")
  if (cards.length < 2) {
    setButtonsState("disable", shuffleButton, nextButton, galleryButton)
  }

  for (let i = 0; i < switchStackButtons.length; i++) {
    if (stacks[i].classList.contains("stack__active")) {
      switchStackButtons[i].classList.add("button-switch-stack--active")
    }
    switchStackButtons[i].classList.remove("button-switch-stack--disable")
  }

  setTimeout(() => {
    state.activeStack.classList.remove("stack__active--spread")
    state.activeStack.parentNode.classList.remove("stack-wrapper--spread")
  }, 1200)
}

function resetGallery() {
  const currentButton = state.activeStack.nextElementSibling
  currentButton.classList.remove("button-switch-stack--gallery")
  state.activeStack.classList.remove("stack__active--gallery")
  state.activeStack.parentNode.classList.remove("stack-wrapper--gallery")
  state.activeStack.parentNode.style.width = "auto"
  gridContainer[0].classList.remove("grid-draggable--active")
  openAreaDraggable.style.zIndex = ""
  disableDraggableInGallery()

  if (hasGridCards()) {
    const cardsInGrid = [...gridContainer[0].children].filter(square => square.children.length)
    cardsInGrid.forEach(square => {
      enableMoveCards(square.firstChild)
      openAreaDraggable.append(square.firstChild)
      openAreaDraggable.classList.add("open-area--active")
    })
    setButtonsState("enable", resetButton)
  } else {
    setButtonsState("enable", galleryButton)
    if (state.activeStack.children.length <= 20) {
      setButtonsState("enable", spreadButton)
    }
  }
  for (let i = 0; i < state.activeStack.children.length; i++) {
    state.activeStack.children[i].style.transition = "margin 2s cubic-bezier(0.75, 0.5, 0.115, 1)";
    state.activeStack.children[i].classList.remove("cd-card__flip");
  }
  setTimeout(() => {
    for (let i = 0; i < state.activeStack.children.length; i++) {
      state.activeStack.children[i].style.transition = ""
    }
    state.activeStack.parentNode.style.width = ""
  }, 2000)
}

function enableDraggable() {
  allCards.forEach(card => {
    card.setAttribute("draggable", true)
    card.addEventListener("dragstart", dragStart)
    card.addEventListener("dragend", dragEnd)
    card.style.cursor = "move"
  })

  stacks.forEach(stack => {
    stack.addEventListener("dragover", onDragOver)
    stack.addEventListener("dragenter", onDragEnterStack)
    stack.addEventListener("dragleave", onDragLeaveStack)
    stack.addEventListener("drop", onDropStack)
  })
  openAreaDraggable.addEventListener("dragover", onDragOver)
  openAreaDraggable.addEventListener("dragenter", onDragEnterOpenArea)
  openAreaDraggable.addEventListener("dragleave", onDragLeaveOpenArea)
  openAreaDraggable.addEventListener("drop", onDropOpenArea)
}

function disableDraggable() {
  allCards.forEach(card => {
    card.setAttribute("draggable", false)
    card.removeEventListener("dragstart", dragStart)
    card.removeEventListener("dragend", dragEnd)
    card.style.cursor = "pointer"
  })
  stacks.forEach(stack => {
    stack.removeEventListener("dragover", onDragOver)
    stack.removeEventListener("dragenter", onDragEnterStack)
    stack.removeEventListener("dragleave", onDragLeaveStack)
    stack.removeEventListener("drop", onDropStack)
  })
  openAreaDraggable.removeEventListener("dragover", onDragOver)
  openAreaDraggable.removeEventListener("dragenter", onDragEnterOpenArea)
  openAreaDraggable.removeEventListener("dragleave", onDragLeaveOpenArea)
  openAreaDraggable.removeEventListener("drop", onDropOpenArea)
}

function enableDraggableInGallery() {
  for (const square of emptySquares) {
    square.addEventListener("dragover", onDragOver)
    square.addEventListener("dragenter", onDragEnterEmptySquare)
    square.addEventListener("dragleave", onDragLeaveEmptySquare)
    square.addEventListener("drop", onDropEmptySquare)
  }
}

function disableDraggableInGallery() {
  for (const square of emptySquares) {
    square.removeEventListener("dragover", onDragOver)
    square.removeEventListener("dragenter", onDragEnterEmptySquare)
    square.removeEventListener("dragleave", onDragLeaveEmptySquare)
    square.removeEventListener("drop", onDropEmptySquare)
  }
}

function dragStart(e) {
  if (e.target.classList.contains("image")) {
    const card = e.target.parentElement.parentElement
    card.classList.add("cd-card--hold")
    setTimeout(() => {
      card.classList.add("cd-card--invisible")
    }, 0)
  } else {
    return
  }
}

function dragEnd(e) {
  const card = e.target.parentElement.parentElement
  card.classList.remove("cd-card--invisible")
  card.classList.remove("cd-card--hold")
}

function onDragOver(e) {
  e.preventDefault()
}

// open area drag and drop functions

function onDragEnterOpenArea(e) {
  e.preventDefault()
  if (e.target.classList.contains("open-area")) {
    e.target.classList.add("open-area--hovered")
  }
}

function onDragLeaveOpenArea(e) {
  e.target.classList.remove("open-area--hovered")
}

function onDropOpenArea(e) {
  const holdedCard = document.getElementsByClassName("cd-card--hold");
  if (holdedCard[0] !== undefined) {
    const parentCard = holdedCard[0].parentNode;
    cardZindex += 1;
    if (e.target.classList.contains("open-area")) {
      e.target.append(holdedCard[0]);
      enableMoveCards(holdedCard[0]);
      setTimeout(() => {
        const card = openAreaDraggable.lastChild;
        disableCardDraggable(card);
      }, 1)
    }
    // keep track of dropped cards
    for (let i = 0; i < stacks.length; i++) {
      if (stacks[i] === parentCard) {
        cardsDroppedInOpenArea[i].push(holdedCard[0])
      }
    }
    if (!e.target.classList.contains("open-area--active")) {
      e.target.classList.add("open-area--active")
    }
    e.target.classList.remove("open-area--hovered")
    setButtonsState("disable", galleryButton, spreadButton)
    setButtonsState("enable", resetButton)
  }
}

// stacks drag and drop functions

function onDragEnterStack(e) {
  e.preventDefault()
  if (e.target.classList.contains("stack")) {
    e.target.classList.add("stack--hovered")
  }
}

function onDragLeaveStack(e) {
  e.target.classList.remove("stack--hovered")
}

function onDropStack(e) {
  const holdedCard = document.getElementsByClassName("cd-card--hold")
  const areCardsInGrid = [...gridContainer[0].children].filter(square => square.children.length)
  // e.target.classList.remove("stack--hovered")
  if (e.target.classList.contains("stack") && holdedCard.length) {
    e.target.append(holdedCard[0])
    e.target.classList.remove("stack--hovered")
    if (state.activeStack.children.length <= 1) {
      setButtonsState("disable", nextButton, shuffleButton, spreadButton)
    } else if (!state.activeStack.classList.contains("stack__active--gallery")) {
      setButtonsState("enable", nextButton, shuffleButton, spreadButton)
    }
    if (!openAreaDraggable.children.length
      && !state.activeStack.classList.contains("stack__active--gallery")
    ) {
      setButtonsState("enable", galleryButton)
      setButtonsState("disable", resetButton)
    } else if (openAreaDraggable.children.length) {
      setButtonsState("disable", spreadButton)
    }
  }
  if (areCardsInGrid.length) {
    if (areCardsInGrid.length === 1 && !state.activeStack.classList.contains("stack__active--gallery")) {
      openAreaDraggable.style.zIndex = ""
      setButtonsState("disable", resetButton)
      setButtonsState("enable", galleryButton, spreadButton)
      gridContainer[0].style.right = ""
      gridContainer[0].style.zIndex = ""
      for (let i = 0; i < cardsDroppedInGrid.length; i++) {
        cardsDroppedInGrid[i] = []
      }
    } else if (areCardsInGrid.length > 1 && !state.activeStack.classList.contains("stack__active--gallery")) {
      setButtonsState("enable", resetButton)
      setButtonsState("disable", galleryButton, spreadButton)
    }
  }

}

// grid drag and drop functions

function onDragEnterEmptySquare(e) {
  e.preventDefault()
  if (e.target.classList.contains("grid-draggable__item")) {
    e.target.classList.add("grid-draggable__item--hovered")
  }
}

function onDragLeaveEmptySquare(e) {
  e.target.classList.remove("grid-draggable__item--hovered")
}

function onDropEmptySquare(e) {
  const holdedCard = document.getElementsByClassName("cd-card--hold")
  if (holdedCard[0] !== undefined) {
    const parentCard = holdedCard[0].parentNode
    e.target.classList.remove("grid-draggable__item--hovered")
    if (!holdedCard[0].classList.contains("image")
      && e.target.children.length < 1
      && !e.target.classList.contains("image")) {
      e.target.append(holdedCard[0])
      if (resetButton.classList.contains("disable")) {
        resetButton.classList.remove("disable")
      }
    }
    for (let i = 0; i < stacks.length; i++) {
      if (stacks[i] === parentCard) {
        cardsDroppedInGrid[i].push(holdedCard[0])
      }
    }
  }
}

// Move card in open area functions
function enableMoveCards(card) {
  const position = { x: 0, y: 0 }

  interact(card).draggable({
    listeners: {
      move(event) {
        position.x += event.dx
        position.y += event.dy

        event.target.style.transform =
          `translate(${position.x}px, ${position.y}px)`
      },
      start(event) {
        card.style.zIndex = cardZindex += 1
      },
    },
    modifiers: [
      interact.modifiers.restrictRect({
        restriction: 'parent'
      })
    ]
  })
}

function disableMoveCards(card) {
  interact(card).unset()
  card.style.zIndex = ""
  cardZindex = 0
}

function disableCardDraggable(card) {
  card.removeEventListener("dragstart", dragStart)
  card.removeEventListener("dragend", dragEnd)
  card.setAttribute("draggable", false)
}

function enableCardDraggable(card) {
  card.addEventListener("dragstart", dragStart)
  card.addEventListener("dragend", dragEnd)
  card.setAttribute("draggable", true)
}

// Debounce function

function debounce(callback, wait) {
  let timerId;
  return (...args) => {
    clearTimeout(timerId);
    timerId = setTimeout(() => {
      callback(...args);
    }, wait);
  };
}

function setButtonsState(action, ...buttons) {
  if (action === "disable") {
    buttons.forEach(button => button.classList.add("disable"))
  } else if (action === "enable") {
    buttons.forEach(button => button.classList.remove("disable"))
  }
}

function hasGridCards() {
  const hasCards = cardsDroppedInGrid.filter(item => item.length)
  if (hasCards.length) {
    return true
  } else {
    return false
  }
}

function doSpreadAnimation() {
  const cards = [...state.activeStack.children].reverse()
  const cardsContainerMarginBottom = Math.ceil(((cards.length) / 5)) * 270
  if (cards.length > 4) {
    state.activeStack.style.marginBottom = `${cardsContainerMarginBottom - 270}px`
  }
  for (let i = 0; i < cards.length; i++) {
    const translateXvalue = cards[i].offsetLeft - 20
    setTimeout(() => {
      if (i === 0) {
        cards[i].style.transform = `translate3d(-${translateXvalue}px, 0, 0)`
      } else if (i >= 1 && i < 5) {
        cards[i].style.transform = `translate3d(-${translateXvalue - (i * 180)}px, 0, 0)`
      } else if (i === 5) {
        cards[i].style.transform = `translate3d(-${translateXvalue}px, ${270}px, 0)`
      } else if (i > 5 && i < 10) {
        cards[i].style.transform = `translate3d(-${translateXvalue - ((i - 5) * 180)}px, ${270}px, 0)`
      } else if (i === 10) {
        cards[i].style.transform = `translate3d(-${translateXvalue}px, ${540}px, 0)`
      } else if (i > 10 && i < 15) {
        cards[i].style.transform = `translate3d(-${translateXvalue - ((i - 10) * 180)}px, ${540}px, 0)`
      } else if (i === 15) {
        cards[i].style.transform = `translate3d(-${translateXvalue}px, ${810}px, 0)`
      } else if (i > 15 && i < 20) {
        cards[i].style.transform = `translate3d(-${translateXvalue - ((i - 15) * 180)}px, ${810}px, 0)`
      } else if (i === 20) {
        cards[i].style.transform = `translate3d(-${translateXvalue}px, ${1080}px, 0)`
      } else if (i > 20 && i < 25) {
        cards[i].style.transform = `translate3d(-${translateXvalue - ((i - 20) * 180)}px, ${1080}px, 0)`
      } else if (i === 25) {
        cards[i].style.transform = `translate3d(-${translateXvalue}px, ${1350}px, 0)`
      } else if (i > 25 && i < 30) {
        cards[i].style.transform = `translate3d(-${translateXvalue - ((i - 25) * 180)}px, ${1350}px, 0)`
      } else if (i === 30) {
        cards[i].style.transform = `translate3d(-${translateXvalue}px, ${1620}px, 0)`
      } else if (i > 30 && i < 35) {
        cards[i].style.transform = `translate3d(-${translateXvalue - ((i - 30) * 180)}px, ${1620}px, 0)`
      } else if (i === 35) {
        cards[i].style.transform = `translate3d(-${translateXvalue}px, ${1890}px, 0)`
      } else if (i > 35 && i < 40) {
        cards[i].style.transform = `translate3d(-${translateXvalue - ((i - 35) * 180)}px, ${1890}px, 0)`
      } else if (i === 40) {
        cards[i].style.transform = `translate3d(-${translateXvalue}px, ${2160}px, 0)`
      } else if (i > 40 && i < 45) {
        cards[i].style.transform = `translate3d(-${translateXvalue - ((i - 40) * 180)}px, ${2160}px, 0)`
      } else if (i === 45) {
        cards[i].style.transform = `translate3d(-${translateXvalue}px, ${2430}px, 0)`
      } else if (i > 45 && i < 50) {
        cards[i].style.transform = `translate3d(-${translateXvalue - ((i - 45) * 180)}px, ${2430}px, 0)`
      } else if (i === 50) {
        cards[i].style.transform = `translate3d(-${translateXvalue}px, ${2700}px, 0)`
      } else if (i > 50 && i < 55) {
        cards[i].style.transform = `translate3d(-${translateXvalue - ((i - 50) * 180)}px, ${2700}px, 0)`
      }
    }, 400 * i)
  }
}

allCards.forEach(card => {
  card.addEventListener("click", onCardClicked);
})

shuffleButton.addEventListener("click", () => {
  const cards = [...state.activeStack.children];

  if (cards.length % 2 === 0) {
    state.activeStack.classList.add("shuffle-even");
  } else {
    state.activeStack.classList.add("shuffle-odd");
  }
  setTimeout(() => {
    for (let i = cards.length; i >= 0; i--) {
      state.activeStack.appendChild(
        state.activeStack.children[(Math.random() * i) | 0]
      );
    }
  }, 200)
  setTimeout(() => {
    state.activeStack.classList.remove("shuffle-even");
    state.activeStack.classList.remove("shuffle-odd");
  }, 700);
});

nextButton.addEventListener("click", debounce(function () {
  let distanceBetweenCards;
  const cards = [...state.activeStack.children];
  const lastIndex = cards.length;
  const lastCard = cards[lastIndex - 1];
  if (state.activeStack.classList.contains("stack__active")) {
    distanceBetweenCards = 11
  } else {
    distanceBetweenCards = 1
  }
  const translateCssValue = (cards.length - 1) * distanceBetweenCards + lastCard.offsetWidth;
  cards.forEach(card => card.style.transition = "transform 1s ease, margin 0s ease-in")
  lastCard.style.transform = `translate3d(${translateCssValue + 10}px, 0, 0) rotate(5deg)`;
  setTimeout(() => {
    lastCard.style.transform = `translateX(-60px)`;
    lastCard.style.zIndex = "-3"
  }, 700);
  setTimeout(() => {
    state.activeStack.prepend(lastCard);
    lastCard.style.zIndex = ""
    cards.forEach(card => card.style.transform = "translateX(-10px)")
  }, 1000);
  setTimeout(() => {
    cards.forEach(card => card.style.transform = "translateX(0)")
  }, 1350);
  setTimeout(() => {
    lastCard.style.transform = "";
    cards.forEach(card => {
      card.style.transform = ""
      card.style.transition = ""
    })
  }, 1360);
}), 1360);

discardButton.addEventListener("click", () => {
  let cards = [...state.activeStack.children]
  let activeCard = cards.filter(item => item.classList.contains("cd-card--active"))

  if (!activeCard.length) {
    errorMessage.innerText = "Select a card to discard"
    setTimeout(() => (errorMessage.innerText = ""), 3000)
  } else {
    activeCard[0].classList.add("cd-card--discard")
    activeCard[0].classList.remove("cd-card--active");
    setButtonsState("enable", resetButton)

    if (state.activeStack.children.length <= 2) {
      setButtonsState("disable", discardButton, nextButton, shuffleButton)
    }

    if (!state.activeStack.classList.contains("stack__active--spread")) {
      setTimeout(() => {
        activeCard[0].parentNode.removeChild(activeCard[0]);
        activeCard[0].classList.remove("cd-card--discard")
      }, 1000)
    }

    for (let i = 0; i < stacks.length; i++) {
      if (state.activeStack.classList.contains(`stack${i + 1}`)) {
        isStackModified[i].status = true
        rejectedCards[i].push(activeCard[0])
      }
    }
  }
})

resetButton.addEventListener("click", () => {
  // if there are cards in open area
  if (openAreaDraggable.children.length) {
    // resetAllDiscardedCards()
    resetCardsFromOpenArea()
  } else if (state.activeStack.classList.contains("stack__active--spread")) {
    // If stack is in spread mode
    resetCardsFromSpread()

  } else if (state.activeStack.classList.contains("stack__active--gallery")) {
    // If stack is in gallery mode
    const cardsLeftInGrid = cardsDroppedInGrid.filter(item => item.length)
    // if there are cards in the grid
    if (cardsLeftInGrid.length) {
      resetCardsFromGrid()
    }
    resetDiscardedCardsFromActiveStack()
  } else {
    // If stack is not in spread nor gallery mode
    resetDiscardedCardsFromActiveStack()
  }
  if (state.activeStack.children.length > 20) {
    setButtonsState("disable", spreadButton)
  }
})



stackButton.addEventListener("click", () => {
  setButtonsState("disable", stackButton)
  if (state.activeStack.classList.contains("stack__active--spread")) {
    resetSpread()
    setButtonsState("enable", spreadButton, galleryButton, shuffleButton, nextButton)
  } else if (state.activeStack.classList.contains("stack__active--gallery")) {
    resetGallery()
    if (state.activeStack.children.length > 1) {
      setButtonsState("enable", shuffleButton, nextButton, discardButton)
    }
  }
})

spreadButton.addEventListener("click", () => {
  setButtonsState("disable", spreadButton, shuffleButton, galleryButton, nextButton)
  setButtonsState("enable", stackButton)
  state.activeStack.classList.add("stack__active--spread")
  state.activeStack.parentNode.classList.add("stack-wrapper--spread")
  openAreaDraggable.style.zIndex = "-2"

  disableDraggable()
  doSpreadAnimation()
  // disble stack buttons
  for (let i = 0; i < switchStackButtons.length; i++) {
    if (switchStackButtons[i].classList.contains("button-switch-stack--active")) {
      switchStackButtons[i].classList.remove("button-switch-stack--active")
    }
    switchStackButtons[i].classList.add("button-switch-stack--disable")
  }
})

galleryButton.addEventListener("click", () => {
  const currentButton = state.activeStack.nextElementSibling
  currentButton.classList.add("button-switch-stack--gallery")
  openAreaDraggable.style.zIndex = "-2"
  setButtonsState("disable", spreadButton, shuffleButton, nextButton, galleryButton)
  setButtonsState("enable", stackButton)
  state.activeStack.classList.add("stack__active--gallery")
  state.activeStack.parentNode.classList.add("stack-wrapper--gallery")
  window.scrollTo(0, 0)
  /* allCards.forEach(card => {
    card.removeEventListener("click", onCardClicked);
    card.style.cursor = "move"
  }) */

  /* for (let i = 0; i < switchStackButtons.length; i++) {
    switchStackButtons[i].classList.add("button-switch-stack--active")
    switchStackButtons[i].style.pointerEvents = "none"
  } */
  setTimeout(() => {
    for (let i = 0; i < state.activeStack.children.length; i++) {
      state.activeStack.children[i].classList.add("cd-card__flip")
    }
    gridContainer[0].classList.add("grid-draggable--active")
    enableDraggableInGallery()
  }, 2000)
})